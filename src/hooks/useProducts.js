import { useState } from "react";
import { message } from "antd";
import useSWR from "swr";
import http from "../services/api-config";
import endpoints from "../services/endpoints";
import useSearch from "./useSearch";
import useModal from "./useModal";
import fetcher from "../services/fetcher";

const useProducts = () => {
  const [page, setPage] = useState(20);
  const { keyword } = useSearch();

  const { data, isLoading } = useSWR(
    keyword
      ? `${endpoints.searchProduct}=${keyword}`
      : `${endpoints.products}?limit=20&skip=${page}`,
    fetcher
  );

  const { setCloseModal, currentProduct, setCloseEditModal } = useModal();

  const createProduct = async (values) => {
    try {
      const response = await http.post(endpoints.addProduct, values);
      message.success(`The ${response.data.title} was successfully created `);
      setCloseModal();
    } catch (error) {
      console.log(error);
      message.error("Failed to create product");
    }
  };

  const editProduct = async (values) => {
    try {
      const response = await http.put(
        `${endpoints.products}/${currentProduct.id}`,
        values
      );
      message.success(`The ${response.data.title} was successfully updated `);
      setCloseEditModal();
    } catch (error) {
      console.log(error);
      message.error("Failed to edit product");
    }
  };

  const deleteProduct = async (id) => {
    try {
      const response = await http.delete(`${endpoints.products}/${id}`);
      message.success(`The ${response.data.title} was successfully deleted `);
    } catch (error) {
      console.log(error);
      message.error("Failed to delete product");
    }
  };

  const handlePage = (value) => {
    setPage(value * 20);
    window.scrollTo(0, 0);
  };

  return {
    data,
    isLoading,
    page,
    handlePage,
    createProduct,
    editProduct,
    deleteProduct,
  };
};

export default useProducts;
