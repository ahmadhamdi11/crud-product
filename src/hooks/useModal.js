import { create } from "zustand";

const useModal = create((set) => ({
  currentProduct: {},
  openModal: false,
  editModal: false,
  setOpenModal: () => set({ openModal: true }),
  setCloseModal: () => set({ openModal: false, currentProduct: {} }),
  setEditModal: (payload) => set({ editModal: true, currentProduct: payload }),
  setCloseEditModal: () => set({ editModal: false, currentProduct: {} }),
}));

export default useModal;
