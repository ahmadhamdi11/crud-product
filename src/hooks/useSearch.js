import { create } from "zustand";

const useSearch = create((set) => ({
  keyword: "",
  setKeyword: (key) => set({ keyword: key }),
}));

export default useSearch;
