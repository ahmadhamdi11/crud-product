import { useState } from "react";
import { useNavigate } from "react-router-dom";
import http from "../services/api-config";
import endpoints from "../services/endpoints";
import { message } from "antd";

export const useLogin = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const logOut = () => {
    localStorage.clear();
    navigate("/login");
  };

  const handleLogin = async (values) => {
    try {
      setLoading(true);
      const { data } = await http.post(endpoints.login, {
        username: values.username,
        password: values.password,
      });
      localStorage.setItem(
        "user",
        JSON.stringify({ username: data.username, image: data.image })
      );
      localStorage.setItem("token", data.token);
      message.success(`Welcome ${data?.username}`);
      navigate("/");
    } catch (error) {
      return message.error("Login Failed");
    } finally {
      setLoading(false);
    }
  };
  return { logOut, handleLogin, loading };
};
