import React from "react";
import getUser from "../utils/getUser";
import { Avatar, Button, Popover } from "antd";
import { LogoutOutlined } from "@ant-design/icons";
import { useLogin } from "../hooks/useLogin";

const ContentHover = ({ event }) => {
  return (
    <div>
      <Button
        style={{ backgroundColor: "#d4380d", color: "#f0f0f0" }}
        onClick={event}
        icon={<LogoutOutlined />}
      >
        Log Out
      </Button>
    </div>
  );
};

const Account = () => {
  const user = getUser();
  const { logOut } = useLogin();
  return (
    <Popover content={<ContentHover event={() => logOut()} />} trigger="hover">
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          gap: 10,
        }}
      >
        <Avatar icon={<img alt="user" src={user?.image} />} />
        <div>{user?.username}</div>
      </div>
    </Popover>
  );
};

export default Account;
