import { Card, Typography, Tag, Badge, Popconfirm, Button } from "antd";
import { EditOutlined, DeleteOutlined, StarOutlined } from "@ant-design/icons";
import formatNumber from "../utils/formatNumber";
import useModal from "../hooks/useModal";
import useProducts from "../hooks/useProducts";

const ProductCard = ({ data }) => {
  const { setEditModal } = useModal();
  const { deleteProduct } = useProducts();
  return (
    <Badge.Ribbon
      text={
        <div>
          {" "}
          <StarOutlined style={{ marginRight: "8px" }} />
          {data?.rating}
        </div>
      }
    >
      <Card
        hoverable
        cover={
          <img
            style={{ height: "150px", objectFit: "contain" }}
            alt={data?.title}
            src={data?.thumbnail}
          />
        }
        style={{ height: "100%" }}
      >
        <Typography.Paragraph
          style={{ fontWeight: "bold" }}
          ellipsis={{ rows: 2, expandable: false, tooltip: data?.title }}
        >
          {data?.title}
        </Typography.Paragraph>
        <Typography.Paragraph style={{ marginTop: "4px", marginBottom: "6px" }}>
          {formatNumber(data?.price)}
        </Typography.Paragraph>
        <Tag color="blue">{data?.category}</Tag>
        <div style={{ display: "flex", gap: 10, marginTop: "10px" }}>
          <Button
            onClick={() => setEditModal(data)}
            icon={<EditOutlined />}
            size="small"
            style={{
              width: "100%",
              backgroundColor: "#faad14",
              borderColor: "#faad14",
              color: "#f0f0f0",
            }}
          />
          <Popconfirm
            title="Delete the product"
            description={`Are you sure to delete this ${data.title}?`}
            onConfirm={() => deleteProduct(data.id)}
          >
            <Button
              icon={<DeleteOutlined />}
              size="small"
              style={{
                width: "100%",
                backgroundColor: "#d4380d",
                borderColor: "#d4380d",
                color: "#f0f0f0",
              }}
            />
          </Popconfirm>
        </div>
      </Card>
    </Badge.Ribbon>
  );
};

export default ProductCard;
