import React from "react";
import { Row, Col } from "antd";
import ProductCard from "./ProductCard";

const ProductList = ({ data }) => {
  return (
    <Row gutter={[14, 10]}>
      {data?.map((product) => (
        <Col xs={12} md={6} lg={4} key={product.id}>
          <ProductCard data={product} />
        </Col>
      ))}
    </Row>
  );
};

export default ProductList;
