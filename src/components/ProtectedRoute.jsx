import React, { useEffect } from "react";
import { getToken } from "../utils/getToken";
import { useNavigate } from "react-router-dom";

const ProtectedRoute = ({ children }) => {
  const token = getToken();
  const navigate = useNavigate();
  useEffect(() => {
    if (!token) {
      return navigate("/login");
    }
  }, [token, navigate]);
  return <div>{children}</div>;
};

export default ProtectedRoute;
