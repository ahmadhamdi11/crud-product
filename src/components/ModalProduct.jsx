import React from "react";
import { Button, Form, Input, Modal } from "antd";

const ModalProduct = ({
  title,
  currentProduct,
  openModal,
  setCloseModal,
  handleSubmit,
}) => {
  return (
    <Modal
      title={title}
      width={500}
      open={openModal}
      onCancel={setCloseModal}
      footer={null}
      destroyOnClose
    >
      <Form
        layout="vertical"
        style={{
          marginTop: "16px",
        }}
        initialValues={currentProduct || {}}
        onFinish={handleSubmit}
      >
        <Form.Item label="Name" name="title" rules={[{ required: true }]}>
          <Input placeholder="Please input name.." />
        </Form.Item>
        <Form.Item label="Price" name="price" rules={[{ required: true }]}>
          <Input type="number" placeholder="Please input price.." />
        </Form.Item>
        <Form.Item
          label="Category"
          name="category"
          rules={[{ required: true }]}
        >
          <Input placeholder="Please input category.." />
        </Form.Item>
        <Form.Item style={{ textAlign: "end" }}>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalProduct;
