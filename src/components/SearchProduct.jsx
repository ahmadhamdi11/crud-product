import React from "react";
import { Form, Input, Button, Space } from "antd";
import {
  SearchOutlined,
  PlusCircleOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import useSearch from "../hooks/useSearch";
import "./layout/main-layout.css";
import useModal from "../hooks/useModal";

const SearchProduct = () => {
  const { setKeyword, keyword } = useSearch();
  const { setOpenModal } = useModal();
  return (
    <Form onFinish={(value) => setKeyword(value.keyword)} layout="inline">
      <Form.Item name="keyword">
        <Input className="search-input" allowClear value={keyword} />
      </Form.Item>
      <Form.Item>
        <Space size={5}>
          <Button type="primary" htmlType="submit" icon={<SearchOutlined />} />
          <Button
            htmlType="button"
            icon={<PlusCircleOutlined />}
            type="primary"
            onClick={setOpenModal}
          />
          <Button
            htmlType="button"
            icon={<ReloadOutlined />}
            type="primary"
            onClick={() => setKeyword("")}
          />
        </Space>
      </Form.Item>
    </Form>
  );
};

export default SearchProduct;
