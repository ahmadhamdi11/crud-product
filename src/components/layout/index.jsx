import React from "react";
import "./main-layout.css";
import { Layout } from "antd";
import { Outlet } from "react-router-dom";

import SearchProduct from "../SearchProduct";
import Account from "../Account";

const { Header, Content, Footer } = Layout;

const MainLayout = () => {
  return (
    <Layout>
      <Header className="layout-header">
        <div className="layout-desktop" style={{ cursor: "pointer" }}>
          Home
        </div>
        <div>
          <SearchProduct />
        </div>
        <div className="layout-desktop">
          <Account />
        </div>
      </Header>
      <Content className="layout-content">
        <Outlet />
      </Content>
      <Footer className="layout-footer">
        &copy;
        <a href="https://ahmadhamdi.netlify.app">Ahmad Hamdi</a>{" "}
      </Footer>
    </Layout>
  );
};

export default MainLayout;
