import React from "react";
import { Button, Card, Form, Input, Typography, Spin } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useLogin } from "../hooks/useLogin";

const Login = () => {
  const { handleLogin, loading } = useLogin();

  return (
    <div
      style={{
        height: "100vh",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        background: "linear-gradient(to right, #fa709a 0%, #fee140 100%)",
      }}
    >
      <Spin spinning={loading} tip="Please wait . . .">
        <Card
          style={{
            width: "400px",
            padding: "16px",
            backgroundColor: "rgba(255, 255, 255, 0.521)",
          }}
        >
          <Typography.Title
            level={3}
            style={{ textAlign: "center", marginBottom: "24px" }}
          >
            LOGIN
          </Typography.Title>
          <Form onFinish={handleLogin}>
            <Form.Item name="username" rules={[{ required: true }]}>
              <Input
                placeholder="Username"
                prefix={<UserOutlined />}
                allowClear
              />
            </Form.Item>
            <Form.Item name="password" rules={[{ required: true }]}>
              <Input.Password
                allowClear
                placeholder="Password"
                prefix={<LockOutlined />}
              />
            </Form.Item>
            <Form.Item>
              <Button
                htmlType="submit"
                type="primary"
                style={{ width: "100%" }}
              >
                Login
              </Button>
            </Form.Item>
          </Form>

          <div style={{ display: "flex", justifyContent: "space-around" }}>
            <div>
              username:{" "}
              <Typography.Text copyable={{ text: "kminchelle" }}>
                kminchelle
              </Typography.Text>
            </div>
            <div>
              password :{" "}
              <Typography.Text copyable={{ text: "0lelplR" }}>
                0lelplR
              </Typography.Text>{" "}
            </div>{" "}
          </div>
        </Card>
      </Spin>
    </div>
  );
};

export default Login;
