import React from "react";
import { Empty, Pagination, Spin } from "antd";
import ProductList from "../components/ProductList";
import useProducts from "../hooks/useProducts";
import ModalProduct from "../components/ModalProduct";
import useModal from "../hooks/useModal";

const Dashboard = () => {
  const { data, isLoading, page, handlePage, createProduct, editProduct } =
    useProducts();
  const {
    currentProduct,
    openModal,
    setCloseModal,
    editModal,
    setCloseEditModal,
  } = useModal();

  return (
    <div>
      {data?.products?.length === 0 ? (
        <Empty />
      ) : (
        <Spin spinning={isLoading} tip="Please wait . . .">
          <div style={{ minHeight: "550px" }}>
            <ProductList data={data?.products} />
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "24px",
            }}
          >
            <Pagination
              total={80}
              pageSize={20}
              onChange={handlePage}
              defaultCurrent={page / 20}
            />
          </div>
          <ModalProduct
            title="Create Product"
            currentProduct={currentProduct}
            openModal={openModal}
            setCloseModal={setCloseModal}
            handleSubmit={createProduct}
          />

          <ModalProduct
            title="Edit Product"
            currentProduct={currentProduct}
            openModal={editModal}
            setCloseModal={setCloseEditModal}
            handleSubmit={editProduct}
          />
        </Spin>
      )}
    </div>
  );
};

export default Dashboard;
