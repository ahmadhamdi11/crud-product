const endpoints = {
  login: "/auth/login",
  products: "/products",
  searchProduct: "/products/search?q",
  addProduct: "/products/add",
};

export default endpoints;
