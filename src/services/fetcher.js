import http from "./api-config";

const fetcher = (url) => http.get(url).then((res) => res.data);

export default fetcher;
